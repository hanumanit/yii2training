<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FixRequest;
use Yii;

/**
 * FixRequestSearch represents the model behind the search form of `common\models\FixRequest`.
 */
class FixRequestSearch extends FixRequest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'request_id', 'fix_group_id', 'receive_by', 'fix_by', 'is_fix', 'is_change_device', 'return_by'], 'integer'],
            [['request_at', 'fix_group_other', 'description', 'receive_at', 'fix_at', 'change_device', 'reason', 'return_at', 'photo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FixRequest::find()->where(['request_id' => Yii::$app->user->getId()]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'request_at' => $this->request_at,
            'request_id' => $this->request_id,
            'fix_group_id' => $this->fix_group_id,
            'receive_by' => $this->receive_by,
            'receive_at' => $this->receive_at,
            'fix_by' => $this->fix_by,
            'fix_at' => $this->fix_at,
            'is_fix' => $this->is_fix,
            'is_change_device' => $this->is_change_device,
            'return_by' => $this->return_by,
            'return_at' => $this->return_at,
        ]);

        $query->andFilterWhere(['like', 'fix_group_other', $this->fix_group_other])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'change_device', $this->change_device])
            ->andFilterWhere(['like', 'reason', $this->reason])
            ->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }
}
