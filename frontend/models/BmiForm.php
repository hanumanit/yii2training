<?php
namespace frontend\models;

use yii\base\Model;

class BmiForm extends Model
{
  public $height;
  public $weight;

  public function rules()
  {
    return [
      [['height', 'weight'], 'required'],
      [['height', 'weight'], 'number']
    ];
  }

  public function attributeLabels()
  {
    return [
      'height' => 'ส่วนสูง (เมตร)',
      'weight' => 'นำ้หนัก (กิโลกรัม)',
    ];
  }

  public function attributeHints()
  {
    return [
      'height' => 'หน่วยเมตร',
      'weight' => 'หน่วยกิโลกรัม',
    ];
  }
}