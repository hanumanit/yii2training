<?php

namespace frontend\themes\manop;

use yii\web\AssetBundle;

class ManopAsset extends AssetBundle
{
  public $sourcePath = '@frontend/themes/manop/assets';

  public $css = [
    'css/fontawesome-all.min.css',
    'css/swiper.min.css',
    'css/animate.min.css',
    'css/odometer.min.css',
    'css/jarallax.css',
    'css/magnific-popup.css',
    'css/bootstrap-select.min.css',
    'css/agrikon-icons.css',
    'css/nouislider.min.css',
    'css/nouislider.pips.css',
    'css/main.css',
  ];

  public $js = [
    'js/swiper.min.js',
    'js/jquery.magnific-popup.min.js',
    'js/jquery.validate.min.js',
    'js/bootstrap-select.min.js',
    'js/jquery.ajaxchimp.min.js',
    'js/wow.js',
    'js/odometer.min.js',
    'js/jquery.appear.min.js',
    'js/jarallax.min.js',
    'js/circle-progress.min.js',
    'js/wNumb.min.js',
    'js/nouislider.min.js',
    'js/theme.js',
  ];

  public $depends = [
    'yii\web\YiiAsset',
    'yii\bootstrap4\BootstrapAsset',
  ];
}