<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Post';

?>

<?php $form = ActiveForm::begin()?>
<?= $form->field($model, 'name')->textInput()?>
<?= $form->field($model, 'description')->textarea()?>

<?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
<?php ActiveForm::end()?>