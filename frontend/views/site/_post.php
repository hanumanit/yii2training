<?php
use yii\helpers\Html;
$assetUrl = Yii::$app->assetManager->getPublishedUrl('@frontend/themes/manop/assets');
?>

<div class="col-md-12 col-lg-4">
  <div class="blog-card">
    <div class="blog-card__image">
      <img src="<?= $assetUrl ?>/images/blog/blog-grid-1.jpg" alt="Best Way to Do Eco and Agriculture">
      <?= Html::a('', ['view', 'id' => $model->id])?>
    </div><!-- /.blog-card__image -->
    <div class="blog-card__content">
      <div class="blog-card__date">18 Nov</div><!-- /.blog-card__date -->
      <div class="blog-card__meta">
        <a href="blog-details.html"><i class="far fa-user-circle"></i> by Admin</a>
        <a href="blog-details.html"><i class="far fa-comments"></i> 2 Comments</a>
      </div><!-- /.blog-card__meta -->
      <h3>
        <?=Html::a($model->name, ['view', 'id' => $model->id])?>
      </h3>
      <?= Html::a('อ่านต่อ', ['view', 'id' => $model->id], ['class' => 'thm-btn'])?><!-- /.thm-btn -->
    </div><!-- /.blog-card__content -->
  </div><!-- /.blog-card -->
</div><!-- /.col-md-12 col-lg-4 -->