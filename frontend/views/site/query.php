<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>

<?= GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'attribute' => 'name',
      'format' => 'raw',
      'value' => function ($model) {
        return '<strong>'.Html::a($model->name, ['view', 'id' => $model->id]).'</strong><br />'.$model->createdBy->username;
      }
    ],
    'description',
    ['class' => 'yii\grid\ActionColumn']
  ]
])?>