<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FixRequest */

$this->title = $model->fixGroup->name;
$this->params['breadcrumbs'][] = ['label' => 'Fix Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fix-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= Html::img($model->getPhoto(), ['class' => 'rounded my-4'])?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'request_at',
            [
                'attribute' => 'request_id',
                'value' => function ($model) {
                    return isset($model->request->staff) ? $model->request->staff->name : null;
                }
            ],
            [
                'attribute' => 'fix_group_id',
                'value' => function ($model) {
                    return $model->fixGroup->name;
                }
            ],
            'fix_group_other',
            'description:ntext',
            [
                'attribute' => 'receive_by',
                'value' => function ($model) {
                    return isset($model->receiveBy) ? $model->receiveBy->staff->name : null;
                }
            ],
            'receive_at',
            [
                'attribute' => 'fix_by',
                'value' => function ($model) {
                    return isset($model->fixBy) ? $model->fixBy->staff->name : null;
                }
            ],
            'fix_at',
            [
                'attribute' => 'is_fix',
                'value' => function ($model) {
                    $arr = [1 => 'สามารถแก้ปัญหาอาการเสียสำเร็จ', 2 => 'ไม่สามารถแก้ปัญหาได้'];
                    return isset($arr[$model->is_fix]) ? $arr[$model->is_fix] : null;
                }
            ],
            'is_change_device',
            'change_device:ntext',
            'reason:ntext',
            [
                'attribute' => 'return_by',
                'value' => function ($model) {
                    return isset($model->returnBy) ? $model->returnBy->staff->name : null;
                }
            ],
            'return_at',
            'photo',
        ],
    ]) ?>

</div>
