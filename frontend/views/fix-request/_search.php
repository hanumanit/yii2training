<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\FixRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fix-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'request_at') ?>

    <?= $form->field($model, 'request_id') ?>

    <?= $form->field($model, 'fix_group_id') ?>

    <?= $form->field($model, 'fix_group_other') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'receive_by') ?>

    <?php // echo $form->field($model, 'receive_at') ?>

    <?php // echo $form->field($model, 'fix_by') ?>

    <?php // echo $form->field($model, 'fix_at') ?>

    <?php // echo $form->field($model, 'is_fix') ?>

    <?php // echo $form->field($model, 'is_change_device') ?>

    <?php // echo $form->field($model, 'change_device') ?>

    <?php // echo $form->field($model, 'reason') ?>

    <?php // echo $form->field($model, 'return_by') ?>

    <?php // echo $form->field($model, 'return_at') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
