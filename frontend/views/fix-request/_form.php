<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\FixGroup;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\FixRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fix-request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->errorSummary($model)?>

    <div class="card shadow mb-5">
        <div class="card-header">
            <h4 class="card-title">ข้อมูลผู้แจ้ง</h4>
        </div>
        <div class="card-body">


            <?= $form->field($model, 'fix_group_id')->radioList(ArrayHelper::map(FixGroup::find()->all(), 'id', 'name')) ?>

            <?php $this->registerJs('
            
            $("input[name=\''.Html::getInputName($model, 'fix_group_id').'\']").change(function(){
                console.log(this.value);
                if(this.value == 6) {
                    $(".fix-group-other").show();
                }else{
                    $(".fix-group-other").hide();
                }
            });
            
            ')?>
            <div class="fix-group-other" <?=!$model->isNewRecord && $model->fix_group_id == 6 ? '' : 'style="display:none;"'?>>
                <?= $form->field($model, 'fix_group_other')->textInput()?>
            </div>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        
            <?= $form->field($model, 'photo[]')->fileInput(['accept' => 'image/*', 'multiple' => true])?>

            <?php if(!$model->isNewRecord){?>
                <?= Html::img($model->getPhoto(), ['class' => 'rounded my-4'])?>
            <?php }?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
