<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\FixRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fix Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fix-request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fix Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img($model->getPhoto(), ['class' => 'rounded']);
                }
            ],
            //'id',
            'request_at',
            //'request_id',
            [
                'attribute' => 'fix_group_id',
                'value' => function ($model) {
                    return $model->fixGroup->name;
                }
            ],
            //'fix_group_other',
            'description:ntext',
            [
                'attribute' => 'receive_by',
                'value' => function($model) {
                    return isset($model->receiveBy) ? $model->receiveBy->staff->name : null;
                }
            ],
            //'receive_at',
            
            [
                'attribute' => 'fix_by',
                'value' => function($model) {
                    return isset($model->fixBy) ? $model->fixBy->staff->name : null;
                }
            ],
            //'fix_at',
            [
                'attribute' => 'is_fix',
                'value' => function ($model) {
                    $arr = [1 => 'สามารถแก้ปัญหาอาการเสียสำเร็จ', 2 => 'ไม่สามารถแก้ปัญหาได้'];
                    return isset($arr[$model->is_fix]) ? $arr[$model->is_fix] : null;
                }
            ],
            //'is_change_device',
            //'change_device:ntext',
            //'reason:ntext',
            [
                'attribute' => 'return_by',
                'value' => function($model) {
                    return isset($model->returnBy) ? $model->returnBy->staff->name : null;
                }
            ],
            //'return_at',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
