<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "staff".
 *
 * @property int $id
 * @property string $name ชื่อ นามสกุล
 * @property string $position ตำแหน่ง
 * @property int $user_id ผู้ใช้งาน
 * @property int $department_id หน่วยงาน
 * @property string $tel โทรศัพท์
 *
 * @property Department $department
 * @property User $user
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'user_id', 'department_id', 'tel'], 'required'],
            [['user_id', 'department_id'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['position', 'tel'], 'string', 'max' => 100],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อ นามสกุล',
            'position' => 'ตำแหน่ง',
            'user_id' => 'ผู้ใช้งาน',
            'department_id' => 'หน่วยงาน',
            'tel' => 'โทรศัพท์',
        ];
    }

    /**
     * Gets query for [[Department]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
