<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fix_group".
 *
 * @property int $id
 * @property string $name ประเภทการซ่อม
 *
 * @property FixRequest[] $fixRequests
 */
class FixGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ประเภทการซ่อม',
        ];
    }

    /**
     * Gets query for [[FixRequests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFixRequests()
    {
        return $this->hasMany(FixRequest::className(), ['fix_group_id' => 'id']);
    }
}
