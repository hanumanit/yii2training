<?php

namespace common\models;

use Imagine\Image\Box;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * This is the model class for table "cms_post".
 *
 * @property int $id
 * @property int $cms_category_id หมวดหมู่
 * @property string $name เรื่อง
 * @property string $description รายละเอียด
 * @property int|null $hit เปิด
 * @property string|null $gallery รูปภาพ
 * @property string|null $file เอกสาร
 * @property int|null $created_at เพิ่มเมื่อ
 * @property int|null $updated_at แก้ไขเมื่อ
 * @property int|null $created_by เพิ่มโดย
 * @property int|null $updated_by แก้ไขโดย
 *
 * @property CmsCategory $cmsCategory
 * @property User $createdBy
 * @property User $updatedBy
 */
class CmsPost extends \yii\db\ActiveRecord
{
    public $uploadGalleryFolder = 'uploads/gallery';
    public $uploadFileFolder = 'uploads/file';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cms_post';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cms_category_id', 'name', 'description'], 'required'],
            [['cms_category_id', 'hit', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 300],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf,doc,docx'],
            [['gallery'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg,jpeg,png,gif', 'maxFiles' => 10],
            [['cms_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CmsCategory::className(), 'targetAttribute' => ['cms_category_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cms_category_id' => 'หมวดหมู่',
            'name' => 'เรื่อง',
            'description' => 'รายละเอียด',
            'hit' => 'เปิด',
            'gallery' => 'รูปภาพ',
            'file' => 'เอกสาร',
            'created_at' => 'เพิ่มเมื่อ',
            'updated_at' => 'แก้ไขเมื่อ',
            'created_by' => 'เพิ่มโดย',
            'updated_by' => 'แก้ไขโดย',
        ];
    }

    /**
     * Gets query for [[CmsCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCmsCategory()
    {
        return $this->hasOne(CmsCategory::className(), ['id' => 'cms_category_id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function uploadGallery($model)
    {
        $files = UploadedFile::getInstances($model, 'gallery');
        if($files) {
            $file_names = [];
            foreach ($files as $file) {
                $file_name = time().'_'.$file->baseName.'.'.$file->extension;
                if($file->saveAs(Yii::getAlias('@webroot').'/'.$this->uploadGalleryFolder.'/'.$file_name)){
                    //Imagine
                    Image::getImagine()->open(Yii::getAlias('@webroot') . '/' . $this->uploadGalleryFolder . '/' . $file_name)
                        ->thumbnail(new Box(600, 600))
                        ->save(Yii::getAlias('@webroot') . '/' . $this->uploadGalleryFolder . '/' . $file_name, ['quality' => 100]);

                    $file_names[] = $file_name;
                }
            }
            return $model->isNewRecord ? implode(',', $file_names) : (!empty($model->gallery) ? implode(',', ArrayHelper::merge($file_names, explode(',', $model->getOldAttribute('gallery')))) : implode(',', $file_names));
        }
        return $model->isNewRecord ? false : $model->getOldAttribute('gallery');
    }

    public function uploadFile($model)
    {
        $file = UploadedFile::getInstance($model, 'file');
        if($file) {
            $file_name = time().'_'.$file->baseName.'.'.$file->extension;
            if($file->saveAs(Yii::getAlias('@webroot').'/'.$this->uploadFileFolder.'/'.$file_name)) {
                return $file_name;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute('file');
    }
}
