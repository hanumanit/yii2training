<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "fix_request".
 *
 * @property int $id
 * @property string $request_at แจ้งซ่อมเมื่อ
 * @property int $request_id ผู้แจ้งซ่อม
 * @property int $fix_group_id ประเภท
 * @property string:null $fix_group_other
 * @property string $description รายละเอียด/สาเหตุ/อาการ
 * @property int|null $receive_by ผู้รับงาน
 * @property string|null $receive_at รับงานเมื่อ
 * @property int|null $fix_by ผู้ซ่อม
 * @property string|null $fix_at ซ่อมเมื่อ
 * @property int|null $is_fix ผลการซ่อม
 * @property int|null $is_change_device การเปลี่ยนอุปกรณ์
 * @property string|null $change_device รายการเปลี่ยนอุปกรณ์
 * @property string|null $reason เหตุผลที่ซ่อมไม่ได้
 * @property int|null $return_by ผู้รับคืน
 * @property string|null $return_at รับคืนเมื่อ
 *
 * @property FixGroup $fixGroup
 * @property User $request
 * @property User $receiveBy
 * @property User $fixBy
 * @property User $returnBy
 */
class FixRequest extends \yii\db\ActiveRecord
{
    public $uploadPhotoFolder = 'uploads/fix/photo';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_at', 'request_id', 'fix_group_id', 'description'], 'required'],
            [['request_at', 'receive_at', 'fix_at', 'return_at'], 'safe'],
            [['request_id', 'fix_group_id', 'receive_by', 'fix_by', 'is_fix', 'is_change_device', 'return_by'], 'integer'],
            [['description', 'change_device', 'reason'], 'string'],
            [['fix_group_other'], 'string', 'max' => 200],
            [['photo'], 'file', 'extensions' => 'jpg,png,gif', 'skipOnEmpty' => true, 'maxFiles' => 10],
            [['fix_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => FixGroup::className(), 'targetAttribute' => ['fix_group_id' => 'id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['request_id' => 'id']],
            [['receive_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['receive_by' => 'id']],
            [['fix_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['fix_by' => 'id']],
            [['return_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['return_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'request_at' => 'แจ้งซ่อมเมื่อ',
            'request_id' => 'ผู้แจ้งซ่อม',
            'fix_group_id' => 'ประเภท',
            'fix_group_other' => 'ประเภทอื่นๆ (ระบุ)',
            'description' => 'รายละเอียด/สาเหตุ/อาการ',
            'receive_by' => 'ผู้รับงาน',
            'receive_at' => 'รับงานเมื่อ',
            'fix_by' => 'ผู้ซ่อม',
            'fix_at' => 'ซ่อมเมื่อ',
            'is_fix' => 'ผลการซ่อม',
            'is_change_device' => 'การเปลี่ยนอุปกรณ์',
            'change_device' => 'รายการเปลี่ยนอุปกรณ์',
            'reason' => 'เหตุผลที่ซ่อมไม่ได้',
            'return_by' => 'ผู้รับคืน',
            'return_at' => 'รับคืนเมื่อ',
            'photo' => 'ภาพประกอบ',
        ];
    }

    /**
     * Gets query for [[FixGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFixGroup()
    {
        return $this->hasOne(FixGroup::className(), ['id' => 'fix_group_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(User::className(), ['id' => 'request_id']);
    }

    /**
     * Gets query for [[ReceiveBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReceiveBy()
    {
        return $this->hasOne(User::className(), ['id' => 'receive_by']);
    }

    /**
     * Gets query for [[FixBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFixBy()
    {
        return $this->hasOne(User::className(), ['id' => 'fix_by']);
    }

    /**
     * Gets query for [[ReturnBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReturnBy()
    {
        return $this->hasOne(User::className(), ['id' => 'return_by']);
    }

    /**
     * Upload Photo
     *
     * @param [[FixRequest]] $model
     * @return void
     */
    public function uploadPhoto($model)
    {
        $files = UploadedFile::getInstances($model, 'photo');
        if($files) {
            $file_names = [];
            foreach ($files as $file) {
                $file_name = time().'_'.$file->baseName.'.'.$file->extension;
                if($file->saveAs($this->getUploadPhotoPath().$file_name)){
                    //
                    $file_names[] = $file_name;
                }
            }
            return $model->isNewRecord ? implode(',', $file_names) : implode(',', ArrayHelper::merge($this->getPhotoToArray($model), $file_names));
        }
        return $model->isNewRecord ? false : $model->getOldAttribute('photo');
    }

    public function getUploadPhotoPath()
    {
        return Yii::getAlias('@webroot').'/'.$this->uploadPhotoFolder.'/';
    }

    public function getPhotoToArray($model)
    {
        return !empty($model->getOldAttribute('photo')) ? explode(',', $model->getOldAttribute('photo')) : [];
    }

    public function getPhoto()
    {
        if(!empty($this->photo)) {
            $photos = explode(',', $this->photo);
            if(count($photos) > 1) {
                return $this->getUploadPhotoUrl().$photos[rand(0, (count($photos) - 1))];
            }else{
                return $this->getUploadPhotoUrl().$photos[0];
            }
        }
        return 'https://via.placeholder.com/350x250';
    }

    public function getPhotos()
    {
        if(!empty($this->photo)) {
            $photos = explode(',', $this->photo);
            $rt = '';
            foreach ($photos as $photo => $value) {
                $rt .= Html::img($this->getUploadPhotoUrl().$value, ['class' => 'rounded m-4']);
            }
            return $rt;
        }
        return Html::img('https://via.placeholder.com/350x250', ['class' => 'rounded']);
    }

    public function getUploadPhotoUrl()
    {
        return Yii::$app->params['frontendUrl'].'/'.$this->uploadPhotoFolder.'/';
    }
}
