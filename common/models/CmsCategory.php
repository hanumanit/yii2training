<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cms_category".
 *
 * @property int $id
 * @property string $name หมวดหมู่
 *
 * @property CmsPost[] $cmsPosts
 */
class CmsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cms_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'หมวดหมู่',
        ];
    }

    /**
     * Gets query for [[CmsPosts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCmsPosts()
    {
        return $this->hasMany(CmsPost::className(), ['cms_category_id' => 'id']);
    }
}
