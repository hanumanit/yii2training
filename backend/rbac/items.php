<?php

return [
    '/admin/*' => [
        'type' => 2,
    ],
    '/cms/*' => [
        'type' => 2,
    ],
    '/hr/*' => [
        'type' => 2,
    ],
    '/manop/*' => [
        'type' => 2,
    ],
    'pms_cms' => [
        'type' => 2,
        'description' => 'Content Management System',
        'children' => [
            '/cms/*',
        ],
    ],
    'pms_administrator' => [
        'type' => 2,
        'description' => 'Administrator',
        'children' => [
            '/admin/*',
        ],
    ],
    'administrator' => [
        'type' => 1,
        'description' => 'Administrator',
        'children' => [
            'pms_administrator',
        ],
    ],
    'cms' => [
        'type' => 1,
        'description' => 'Content Management System',
        'children' => [
            'pms_cms',
        ],
    ],
    'hr' => [
        'type' => 1,
        'description' => 'HR',
        'children' => [
            'pms_hr',
        ],
    ],
    'manop' => [
        'type' => 1,
        'description' => 'Manop Fix',
        'children' => [
            'pms_manop',
        ],
    ],
    'pms_hr' => [
        'type' => 2,
        'description' => 'HR Permission',
        'children' => [
            '/hr/*',
        ],
    ],
    'pms_manop' => [
        'type' => 2,
        'description' => 'Manop Fix Permission',
        'children' => [
            '/manop/*',
        ],
    ],
];
