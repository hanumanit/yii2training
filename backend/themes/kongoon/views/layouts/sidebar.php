<?php
use yii\helpers\Html;
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=Yii::$app->homeUrl?>" class="brand-link">
        <img src="<?=$assetDir?>/img/AdminLTELogo.png" alt="Fix" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Fix</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?=$assetDir?>/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?=Yii::$app->user->identity->staff->name?></a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            $menuItems = [
    ['label' => 'Home', 'url' => ['/site/index']],
  ];
  if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
  } else {
    
    if (Yii::$app->user->can('cms')) {
      $menuItems[] = ['label' => 'CMS', 'url' => ['/cms'], 'items' => [
        ['label' => 'Category', 'url' => ['/cms/category/index']],
        ['label' => 'Post', 'url' => ['/cms/post/index']],
      ]];
    }

    if(Yii::$app->user->can('hr')) {
      $menuItems[] = ['label' => 'HR', 'url' => ['/hr'], 'items' => [
        ['label' => 'Department', 'url' => ['/hr/department/index']],
        ['label' => 'Staff', 'url' => ['/hr/staff/index']],
      ]];
    }

    if(Yii::$app->user->can('manop')) {
      $menuItems[] = ['label' => 'Fix', 'url' => ['/manop'], 'items' => [
        ['label' => 'Fix Request', 'url' => ['/manop/fix-request/index']],
        ['label' => 'Fix Group', 'url' => ['/manop/fix-group/index']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท', 'url' => ['/manop/report/report1']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y'), 'url' => ['/manop/report/report2']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท', 'url' => ['/manop/report/report3']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y'), 'url' => ['/manop/report/report4']],
        //['label' => 'Report5', 'url' => ['/manop/report/report5']],
      ]];
    }

    if (Yii::$app->user->can('administrator')) {
      $menuItems[] = ['label' => 'Administrator', 'url' => ['/admin'], 'items' => [
        ['label' => 'Assignment', 'url' => ['/admin/assignment']],
        ['label' => 'Role', 'url' => ['/admin/role']],
        ['label' => 'Permission', 'url' => ['/admin/permission']],
        ['label' => 'Route', 'url' => ['/admin/route']],
      ]];
    }

    
  }
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => $menuItems,
            ]);
            
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>