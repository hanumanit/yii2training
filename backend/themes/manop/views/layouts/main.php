<?php

use backend\themes\manop\ManopAsset;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use common\widgets\Alert;

ManopAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
</head>

<body>
  <?php $this->beginBody() ?>

  <?php
  NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
      'class' => 'navbar-expand-lg navbar-light bg-light',
    ],
  ]);
  $menuItems = [
    ['label' => 'Home', 'url' => ['/site/index']],
  ];
  if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
  } else {
    
    if (Yii::$app->user->can('cms')) {
      $menuItems[] = ['label' => 'CMS', 'url' => ['/cms'], 'items' => [
        ['label' => 'Category', 'url' => ['/cms/category/index']],
        ['label' => 'Post', 'url' => ['/cms/post/index']],
      ]];
    }

    if(Yii::$app->user->can('hr')) {
      $menuItems[] = ['label' => 'HR', 'url' => ['/hr'], 'items' => [
        ['label' => 'Department', 'url' => ['/hr/department/index']],
        ['label' => 'Staff', 'url' => ['/hr/staff/index']],
      ]];
    }

    if(Yii::$app->user->can('manop')) {
      $menuItems[] = ['label' => 'Fix', 'url' => ['/manop'], 'items' => [
        ['label' => 'Fix Request', 'url' => ['/manop/fix-request/index']],
        ['label' => 'Fix Group', 'url' => ['/manop/fix-group/index']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท', 'url' => ['/manop/report/report1']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y'), 'url' => ['/manop/report/report2']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท', 'url' => ['/manop/report/report3']],
        ['label' => 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y'), 'url' => ['/manop/report/report4']],
        //['label' => 'Report5', 'url' => ['/manop/report/report5']],
      ]];
    }

    if (Yii::$app->user->can('administrator')) {
      $menuItems[] = ['label' => 'Administrator', 'url' => ['/admin'], 'items' => [
        ['label' => 'Assignment', 'url' => ['/admin/assignment']],
        ['label' => 'Role', 'url' => ['/admin/role']],
        ['label' => 'Permission', 'url' => ['/admin/permission']],
        ['label' => 'Route', 'url' => ['/admin/route']],
      ]];
    }

    $menuItems[] = '<li>'
      . Html::beginForm(['/site/logout'], 'post')
      . Html::submitButton(
        'Logout (' . Yii::$app->user->identity->username . ')',
        ['class' => 'btn btn-link logout']
      )
      . Html::endForm()
      . '</li>';
  }
  echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
  ]);
  NavBar::end();
  ?>

  <div class="container">
    <div class="mt-4"></div>
    <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    <?= \diecoding\toastr\ToastrFlash::widget([
      'options' => [
        'newestOnTop' => true,
        'showDuration' => 1000,
        'hideDuration' => 1000,
        'timeOut' => 10000,
        'closeButton' => true,
        'progressBar' => true,
      ]
    ]) ?>
    <?= $content ?>
  </div>
  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>