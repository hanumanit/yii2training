<?php
namespace backend\themes\manop;

use yii\web\AssetBundle;

class ManopAsset extends AssetBundle
{
  public $sourcePath = '@backend/themes/manop/assets';

  public $css = [
    'css/manop.css'
  ];

  public $js = [
    'js/manop.js'
  ];

  public $depends = [
    'yii\web\JqueryAsset',
    'yii\bootstrap4\BootstrapAsset',
  ];
}