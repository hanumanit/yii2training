<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CmsPost */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'เรื่อง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cms-post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            [
                'attribute' => 'cms_category_id',
                'value' => function ($model) {
                    return $model->cmsCategory->name;
                }
            ],
            'name',
            'description:html',
            'hit',
            'gallery',
            'file',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return $model->createdBy->username;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return $model->updatedBy->username;
                }
            ],
        ],
    ]) ?>

    <div class="row">
        <?php
        $gallery_files = !empty($model->gallery) ? explode(',', $model->gallery) : false;
        if ($gallery_files) {
            foreach ($gallery_files as $file) {
                echo '<div class="col-md-3">'.Html::img(Yii::getAlias('@web') . '/' . $model->uploadGalleryFolder . '/' . $file, ['class' => 'img-responsive']).'</div>';
            }
        }
        ?>
    </div>

</div>