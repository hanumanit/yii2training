<?php

use common\models\CmsCategory;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\cms\models\CmsPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เรื่อง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cms-post-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มเรื่อง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'cms_category_id',
                'filter' => ArrayHelper::map(CmsCategory::find()->all(), 'id', 'name'),
                'value' => function ($model) {
                    return $model->cmsCategory->name;
                }
            ],
            'name',
            //'description:html',
            'hit',
            //'gallery',
            //'file',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return $model->createdBy->username;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return $model->updatedBy->username;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
