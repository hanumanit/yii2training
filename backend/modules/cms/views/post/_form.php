<?php

use common\models\CmsCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\CmsPost */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cms-post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cms_category_id')->dropDownList(ArrayHelper::map(CmsCategory::find()->all(), 'id', 'name'), ['prompt' => 'เลือกหมวดหมู่...']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::class, [
        'options' => ['rows' => 6],
        'preset' => 'full'
    ]) ?>

    <?= $form->field($model, 'gallery[]')->fileInput(['multiple' => true]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
