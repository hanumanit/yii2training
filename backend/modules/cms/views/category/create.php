<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CmsCategory */

$this->title = 'เพิ่มหมวดหมู่';
$this->params['breadcrumbs'][] = ['label' => 'หมวดหมู่', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cms-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
