<?php

namespace backend\modules\manop\controllers;
use Yii;
use kartik\mpdf\Pdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use common\models\FixRequest;
use yii\web\NotFoundHttpException;

class ReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionReport1()
    {
        $result = Yii::$app->db->createCommand("
        SELECT fg.name, COUNT(fr.id) AS cnt
        FROM fix_request fr
        LEFT JOIN fix_group fg ON fg.id = fr.fix_group_id
        GROUP BY fr.fix_group_id
        ")->queryAll();

        //var_dump($result);
        $categories = [];
        $datas = [];
        foreach ($result as $data) {
            $categories[] = $data['name'];
            $datas[] = (int) $data['cnt'];
        }

        return $this->render('report1', [
            'categories' => $categories,
            'datas' => $datas,
        ]);
    }

    public function actionReport2()
    {
        $result = Yii::$app->db->createCommand("
        SELECT MONTHNAME(request_at) AS mm, COUNT(fr.id) AS cnt
        FROM fix_request fr
        LEFT JOIN fix_group fg ON fg.id = fr.fix_group_id
        WHERE year(request_at) = year(NOW())
        GROUP BY mm
        ")->queryAll();

        $categories = [];
        $datas = [];
        foreach($result as $data) {
            $categories[] = $data['mm'];
            $datas[] = (int) $data['cnt'];
        }

        return $this->render('report2', [
            'categories' => $categories,
            'datas' => $datas,
        ]);
    }

    public function actionReport3()
    {
        $result = Yii::$app->db->createCommand("
        SELECT fg.name, COUNT(fr.id) AS cnt
        FROM fix_request fr
        LEFT JOIN fix_group fg ON fg.id = fr.fix_group_id
        GROUP BY fr.fix_group_id
        ")->queryAll();

        //var_dump($result);
        $datas = [];
        foreach ($result as $data) {
            $datas[] = ['name' => $data['name'], 'y' => (int) $data['cnt']];
        }

        return $this->render('report3', [
            'datas' => $datas,
        ]);
    }

    public function actionReport4()
    {
        
        $result = Yii::$app->db->createCommand("
        SELECT fg.name AS name,
        SUM(CASE WHEN month(request_at) = 1 THEN 1 ELSE 0 END) AS m1,
        SUM(CASE WHEN month(request_at) = 2 THEN 1 ELSE 0 END) AS m2,
        SUM(CASE WHEN month(request_at) = 3 THEN 1 ELSE 0 END) AS m3,
        SUM(CASE WHEN month(request_at) = 4 THEN 1 ELSE 0 END) AS m4,
        SUM(CASE WHEN month(request_at) = 5 THEN 1 ELSE 0 END) AS m5,
        SUM(CASE WHEN month(request_at) = 6 THEN 1 ELSE 0 END) AS m6,
        SUM(CASE WHEN month(request_at) = 7 THEN 1 ELSE 0 END) AS m7,
        SUM(CASE WHEN month(request_at) = 8 THEN 1 ELSE 0 END) AS m8,
        SUM(CASE WHEN month(request_at) = 9 THEN 1 ELSE 0 END) AS m9,
        SUM(CASE WHEN month(request_at) = 10 THEN 1 ELSE 0 END) AS m10,
        SUM(CASE WHEN month(request_at) = 11 THEN 1 ELSE 0 END) AS m11,
        SUM(CASE WHEN month(request_at) = 12 THEN 1 ELSE 0 END) AS m12
        FROM fix_request fr
        LEFT JOIN fix_group fg ON fg.id = fr.fix_group_id
        WHERE year(request_at) = year(NOW())
        GROUP BY fg.id
        ")->queryAll();

        $categories = [];
        $datas = [];
        foreach($result as $data) {
            $datas[] = ['name' => $data['name'], 'data' => [(int) $data['m1'], (int) $data['m2'], (int) $data['m3'], (int) $data['m4'],(int) $data['m5'], (int) $data['m6'], (int) $data['m7'], (int) $data['m8'], (int) $data['m9'], (int) $data['m10'], (int) $data['m11'], (int) $data['m12']]];
        }

        return $this->render('report4', [
            'datas' => $datas,
        ]);
    }

    public function actionReport5($id)
    {
        $model = FixRequest::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException('ไม่พบ Case นี้');
        }
        $content = $this->renderPartial('report5', [
            'model' => $model
        ]);
    
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_UTF8, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        // format content from your own css file if needed or use the
        // enhanced bootstrap css built by Krajee for mPDF formatting 
        'cssFile' => '@backend/web/css/pdf.css',
        // any css to be embedded if required
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => [
            'title' => 'ใบแจ้งซ่อมคอมพิวเตอร์ #'.$model->id,
            'showWatermarkText' => true,
            'watermark_font' => 'sarabun',
        ],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetWatermarkText' => ['สถาบันพยาธิวิทยา', 0.05],
            'SetHeader'=>['ใบแจ้งซ่อมคอมพิวเตอร์ #'.$model->id], 
            'SetFooter'=>['
            <table width="100%">
                <tr>
                    <td width="33%" class="set-font13">{DATE d-m-Y}</td>
                    <td width="33%" align="center" class="set-font13">{PAGENO}/{nbpg}</td>
                    <td width="33%" style="text-align: right;" class="set-font13">ใบแจ้งซ่อมคอมพิวเตอร์ #'.$model->id.'</td>
                </tr>
            </table>
            '],
        ]
    ]);

    $defaultConfig = (new ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];

    $defaultFontConfig = (new FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];

    $pdf->options['fontDir'] = array_merge($fontDirs, [
            Yii::getAlias('@webroot').'/fonts',
    ]);
    $pdf->options['fontdata'] = $fontData + [
            'sarabun' => [
                'R' => 'sarabun.ttf',
            ]
        ];
    //$pdf->options['default_font'] = 'sarabun';
    
    // return the pdf output as per the destination setting
    return $pdf->render(); 
    }

}
