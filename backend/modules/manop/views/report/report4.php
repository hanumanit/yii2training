<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;

$this->title = 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y');
?>
<?=Highcharts::widget([
    'options' => [
        'chart' => ['type' => 'line'],
        'title' => ['text' => $this->title],
        'xAxis' => [
            'categories' => ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.']
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => $datas
   ]
])?>