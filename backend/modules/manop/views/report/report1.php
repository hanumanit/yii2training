<?php
/* @var $this yii\web\View */

use miloschuman\highcharts\Highcharts;

$this->title = 'จำนวนการแจ้งซ่อมแบ่งตามประเภท';
?>

<?=Highcharts::widget([
    'options' => [
        'title' => ['text' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท'],
        'xAxis' => [
            'categories' => $categories
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            ['name' => 'ประเภท', 'data' => $datas],
        ]
    ]
])?>
