<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;

$this->title = 'จำนวนการแจ้งซ่อมแบ่งตามเดือนในปี '.date('Y');
?>
<?=Highcharts::widget([
    'options' => [
        'chart' => ['type' => 'column'],
        'title' => ['text' => $this->title],
        'xAxis' => [
            'categories' => $categories
        ],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            ['name' => 'จำนวน', 'data' => $datas],
        ]
   ]
])?>