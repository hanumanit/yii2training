<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<div class="" style="text-align: center;">
<?=Html::img('https://www.iop.or.th/img/logo.png', ['width' => 80])?>
</div>
<h1 style="text-align: center;">ใบแจ้งซ่อมคอมพิวเตอร์</h1>
<div style="text-align: right;">วันที่ <?=Yii::$app->formatter->asDateTime($model->request_at)?></div>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background-color: #999999;" colspan="2">ส่วนที่ 1 ข้อมูลผู้แจ้ง</td>
  </tr>
  <tr>
    <td>ชื่อ-นามสกุล <?=$model->request->staff->name?></td>
    <td>ตำแหน่ง <?=$model->request->staff->position?></td>
  </tr>
  <tr>
    <td>หน่วยงาน <?=$model->request->staff->department->name?></td>
    <td>โทรศัพท์ติดต่อ <?=$model->request->staff->tel?></td>
  </tr>
</table>

<div class="">
  แจ้งปัญหา <?=$model->fixGroup->name?>
</div>
<?php if(!empty($model->fix_group_other)) {?>
<p>
  <?=$model->fix_group_other?>
</p>
<?php }?>
<p>
รายละเอียดปัญหา: <?=$model->description?>
</p>
<p>
<?php if(!empty($model->photo)){
  $photos = explode(',', $model->photo);
  foreach ($photos as $key => $photo) {
    echo Html::img($model->getUploadPhotoUrl().$photo, ['width' => 200]); //Yii::getAlias('@webroot')
  }
}?>
</p>
<div style="text-align: right;">
  <p>
  ลงชื่อ <?=$model->request->staff->name?> ผู้แจ้งซ่อม<br />
  ลงชื่อ <?=isset($model->receiveBy) ? $model->receiveBy->staff->name : '' ?> ผู้รับแจ้ง
  </p>
</div>


<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background-color: #999999;" colspan="2">ส่วนที่ 2 สำหรับเจ้าหน้าที่</td>
  </tr>
</table>
<p>
  ผลการซ่อม: <?= !empty($model->is_fix) ? ($model->is_fix == 1 ? 'สามารถแก้ปัญหาอาการเสียสำเร็จ' : ('ไม่สามารถแก้ปัญหาได้ สาเหตุ '.$model->reason)) : ''?><br />

<?=$model->is_fix == 1 && $model->is_change_device == 1 ? ('ต้องเปลี่ยนอุปกรณ์<br />'.$model->change_device) : 'ไม่ต้องเปลี่ยนอุปกรณ์'?>
</p>

<p>
ลงชื่อ <?=isset($model->fixBy) ? $model->fixBy->staff->name : ''?> ผู้ซ่อม<br />
ลงชื่อ <?=isset($model->returnBy) ?  $model->returnBy->staff->name : ''?> ผู้รับคืน วันที่ <?=$model->return_at?>
</p>
