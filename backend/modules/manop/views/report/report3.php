<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;

$this->title = 'จำนวนการแจ้งซ่อมแบ่งตามประเภท';
?>

<?=Highcharts::widget([
    'options' => [
        'chart' => [
            'type' => 'pie',
            'options3d' =>  [
                'enabled' =>  true,
                'alpha' => 45,
                'beta' =>  0
            ]
    
        ],
        'title' => ['text' => 'จำนวนการแจ้งซ่อมแบ่งตามประเภท'],
        'yAxis' => [
            'title' => ['text' => 'จำนวน']
        ],
        'series' => [
            ['name' => 'จำนวน', 'data' => $datas],
        ]
    ]
])?>