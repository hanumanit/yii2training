<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FixRequest */

$this->title = 'Create Fix Request';
$this->params['breadcrumbs'][] = ['label' => 'Fix Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fix-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
