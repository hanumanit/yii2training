<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\FixGroup;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\FixRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fix-request-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=$form->errorSummary($model)?>

    <div class="card shadow mb-5">
        <div class="card-header">
            <h4 class="card-title">ข้อมูลผู้แจ้ง</h4>
        </div>
        <div class="card-body">

             <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'request_at')->widget(DateTimePicker::class, [
                        'options' => ['placeholder' => 'Enter date time ...'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:00', //Y-m-d H:i:s
                        ]
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'request_id')->widget(Select2::class, [
                        'initValueText' => $model->isNewRecord ? '' : $model->request->staff->name,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        //'data' => ArrayHelper::map(User::find()->where(['status' => User::STATUS_ACTIVE])->all(), 'id', function($model) {return $model->staff->name;}),
                        'pluginOptions' => [
                            'placeholder' => 'Select Staff...',
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => Url::to(['/site/get-user']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ]
                        ],
                        
                    ]) ?>
                </div>
             </div>

            <?= $form->field($model, 'fix_group_id')->radioList(ArrayHelper::map(FixGroup::find()->all(), 'id', 'name')) ?>

            <?php $this->registerJs('
            
            $("input[name=\''.Html::getInputName($model, 'fix_group_id').'\']").change(function(){
                console.log(this.value);
                if(this.value == 6) {
                    $(".fix-group-other").show();
                }else{
                    $(".fix-group-other").hide();
                }
            });
            
            ')?>
            <div class="fix-group-other" <?=!$model->isNewRecord && $model->fix_group_id == 6 ? '' : 'style="display:none;"'?>>
                <?= $form->field($model, 'fix_group_other')->textInput()?>
            </div>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'receive_by')->widget(Select2::class, [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'initValueText' => !$model->isNewRecord && !empty($model->receive_by) ?  $model->receiveBy->staff->name : '',
                        //'data' => ArrayHelper::map(User::find()->where(['status' => User::STATUS_ACTIVE])->all(), 'id', function($model) {return $model->staff->name;}),
                        'pluginOptions' => [
                            'placeholder' => 'Select Staff...',
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => Url::to(['/site/get-user']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ]
                        ],
                        
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'receive_at')->widget(DateTimePicker::class, [
                        'options' => ['placeholder' => 'Enter date time ...'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:00', //Y-m-d H:i:s
                        ]
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
    

    <div class="card shadow mb-5">
        <div class="card-header">
            <h4 class="card-title">สำหรับเจ้าหน้าที่</h4>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'fix_by')->widget(Select2::class, [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'initValueText' => !$model->isNewRecord && !empty($model->fix_by) ?  $model->fixBy->staff->name : '',
                        //'data' => ArrayHelper::map(User::find()->where(['status' => User::STATUS_ACTIVE])->all(), 'id', function($model) {return $model->staff->name;}),
                        'pluginOptions' => [
                            'placeholder' => 'Select Staff...',
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => Url::to(['/site/get-user']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ]
                        ],
                        
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'fix_at')->widget(DateTimePicker::class, [
                        'options' => ['placeholder' => 'Enter date time ...'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:00', //Y-m-d H:i:s
                        ]
                    ]) ?>
                </div>
            </div>
            
            <?= $form->field($model, 'is_fix')->radioList([1 => 'สามารถแก้ปัญหาอาการเสียสำเร็จ', 2 => 'ไม่สามารถแก้ปัญหาได้']) ?>

            <?php $this->registerJs('
            
            $("input[name=\''.Html::getInputName($model, 'is_fix').'\']").change(function(){
                console.log(this.value);
                if(this.value == 2) {
                    $(".reason").show();
                    $(".is_change_device").hide();
                }else{
                    $(".reason").hide();
                    $(".is_change_device").show();
                }
            });
            
            ')?>

            <div class="reason" <?=!$model->isNewRecord && $model->is_fix == 2 ? '' : 'style="display:none;"'?>>
                <?= $form->field($model, 'reason')->textarea(['rows' => 6]) ?>
            </div>

            <div class="is_change_device" <?=!$model->isNewRecord && $model->is_fix == 1 ? '' : 'style="display:none"'?>>

                <?= $form->field($model, 'is_change_device')->radioList([1 => 'ต้องเปลี่ยนอุปกรณ์', 2 => 'ไม่ต้องเปลี่ยนอุปกรณ์']) ?>
                <?php $this->registerJs('
                $("input[name=\''.Html::getInputName($model, 'is_change_device').'\']").change(function(){
                    console.log(this.value);
                    if(this.value == 1) {
                        $(".change_device").show();
                    }else{
                        $(".change_device").hide();
                    }
                });
                ')?>
                <div class="change_device" <?=!$model->isNewRecord && $model->is_change_device == 1 ? '' : 'style="display:none"'?>>
                    <?= $form->field($model, 'change_device')->textarea(['rows' => 6]) ?>
                </div>

            </div>
        
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'return_by')->widget(Select2::class, [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'initValueText' => !$model->isNewRecord && !empty($model->return_by) ?  $model->returnBy->staff->name : '',
                        //'data' => ArrayHelper::map(User::find()->where(['status' => User::STATUS_ACTIVE])->all(), 'id', function($model) {return $model->staff->name;}),
                        'pluginOptions' => [
                            'placeholder' => 'Select Staff...',
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => Url::to(['/site/get-user']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ]
                        ],
                        
                    ]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'return_at')->widget(DateTimePicker::class, [
                        'options' => ['placeholder' => 'Enter date time ...'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:00', //Y-m-d H:i:s
                        ]
                    ]) ?>
                </div>
            </div>

                    
        </div>
    </div>

    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
