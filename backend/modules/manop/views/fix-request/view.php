<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\FixRequest */

$this->title = $model->fixGroup->name;
$this->params['breadcrumbs'][] = ['label' => 'Fix Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fix-request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= $model->getPhotos()?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'request_at',
            [
                'attribute' => 'request_id',
                'value' => function($model) {
                    return $model->request->staff->name;
                }
            ],
            [
                'attribute' => 'fix_group_id',
                'value' => function ($model) {
                    return $model->fixGroup->name;
                }
            ],
            'description:ntext',
            [
                'attribute' => 'receive_by',
                'value' => function ($model) {
                    return isset($model->receiveBy) ? $model->receiveBy->staff->name : null;
                }
            ],
            'receive_at',
            [
                'attribute' => 'fix_by',
                'value' => function ($model) {
                    return isset($model->fixBy) ? $model->fixBy->staff->name : null;
                }
            ],
            'fix_at',
            [
                'attribute' => 'is_fix',
                'value' => function ($model) {
                    $arr = [1 => 'สามารถแก้ปัญหาอาการเสียสำเร็จ', 2 => 'ไม่สามารถแก้ปัญหาได้'];
                    return isset($arr[$model->is_fix]) ? $arr[$model->is_fix] : null;
                }
            ],
            [
                'attribute' => 'is_change_device',
                'value' => function ($model) {
                    return !empty($model->is_change_device) && $model->is_change_device == 1 ? 'เปลี่ยนอุปกรณ์' : 'ไม่ได้เปลี่ยนอุปกรณ์';
                }
            ],
            'change_device:ntext',
            'reason:ntext',
            [
                'attribute' => 'return_by',
                'value' => function ($model) {
                    return isset($model->returnBy) ? $model->returnBy->staff->name : null;
                }
            ],
            'return_at',
            
        ],
    ]) ?>

    <iframe src="<?=Url::to(['/manop/report/report5', 'id' => $model->id])?>" title="" width="100%" height="800" frameborder="0"></iframe>

</div>
