<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\manop\models\FixRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fix Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fix-request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fix Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'photo',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img($model->getPhoto(), ['class' => 'rounded']);
                }
            ],
            //'id',
            'request_at',
            [
                'attribute' => 'request_id',
                'value' => function ($model) {
                    return $model->request->staff->name;
                }
            ],
            [
                'attribute' => 'fix_group_id',
                'value' => function ($model) {
                    return $model->fixGroup->name;
                }
            ],
            'description:ntext',
            //'receive_by',
            //'receive_at',
            //'fix_by',
            //'fix_at',
            //'is_fix',
            //'is_change_device',
            //'change_device:ntext',
            //'reason:ntext',
            //'return_by',
            //'return_at',
            //'photo',
            [
                'label' => 'ใบแจ้งซ่อม',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('ใบแจ้งซ่อม', ['/manop/report/report5', 'id' => $model->id], ['class' => 'btn btn-sm btn-warning', 'target' => '_blank',]); // 'data' => ['confirm' => 'ต้องการเปิด PDF ไหม?']
                }
            ],
            [
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('รับงาน', ['receive', 'id' => $model->id], ['class' => 'btn btn-sm btn-success']).
                        (!empty($model->receive_by) ? '<br /><small>'.$model->receiveBy->staff->name.'<br />'.$model->receive_at.'</small>' : '');
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
