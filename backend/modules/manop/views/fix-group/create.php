<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FixGroup */

$this->title = 'Create Fix Group';
$this->params['breadcrumbs'][] = ['label' => 'Fix Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fix-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
