-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 23, 2021 at 02:23 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2_iop_training`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_category`
--

CREATE TABLE `cms_category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT 'หมวดหมู่'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_category`
--

INSERT INTO `cms_category` (`id`, `name`) VALUES
(1, 'ข่าวประชาสัมพันธ์'),
(2, 'ข่าวกิจกรรม');

-- --------------------------------------------------------

--
-- Table structure for table `cms_post`
--

CREATE TABLE `cms_post` (
  `id` int(11) NOT NULL,
  `cms_category_id` int(11) NOT NULL COMMENT 'หมวดหมู่',
  `name` varchar(300) COLLATE utf8_unicode_ci NOT NULL COMMENT 'เรื่อง',
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'รายละเอียด',
  `hit` int(11) DEFAULT '0' COMMENT 'เปิด',
  `gallery` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'รูปภาพ',
  `file` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'เอกสาร',
  `created_at` int(11) DEFAULT NULL COMMENT 'เพิ่มเมื่อ',
  `updated_at` int(11) DEFAULT NULL COMMENT 'แก้ไขเมื่อ',
  `created_by` int(11) DEFAULT NULL COMMENT 'เพิ่มโดย',
  `updated_by` int(11) DEFAULT NULL COMMENT 'แก้ไขโดย'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cms_post`
--

INSERT INTO `cms_post` (`id`, `cms_category_id`, `name`, `description`, `hit`, `gallery`, `file`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(4, 1, 'ทดสอบการเพิ่มข่าวประชาสัมพันธ์', '<p><strong>ทดสอบการเพิ่มข่าวประชาสัมพันธ์</strong></p>\r\n', 0, '', '', 1619079454, 1619079454, 1, 1),
(5, 1, 'test', '<p>test</p>\r\n', 0, '1619082973_9375.jpg,1619082974_9376.jpg,1619082507_9374.jpg', '', 1619082507, 1619082974, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1618987904),
('m130524_201442_init', 1618987925),
('m190124_110200_add_verification_token_column_to_user_table', 1618987925);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'lkWFVPnM63k4uIHCxqnLbLsOHiDJX6mL', '$2y$13$Uh4Wrru2Tse9i3TPJ3GPWu8Qgy4mLKUYYnAy5dez6HuDslxvS.Q8a', NULL, 'admin@admin.com', 10, 1618988008, 1618988008, 'UScqaKbkxlGfF-o1OXlj167MvFFrBHfv_1618988008');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_category`
--
ALTER TABLE `cms_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_post`
--
ALTER TABLE `cms_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_post_created_by_user_id` (`created_by`),
  ADD KEY `idx_post_updated_by_user_id` (`updated_by`),
  ADD KEY `idx_cms_post_cms_category_idx` (`cms_category_id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_category`
--
ALTER TABLE `cms_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_post`
--
ALTER TABLE `cms_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_post`
--
ALTER TABLE `cms_post`
  ADD CONSTRAINT `fk_cms_post_cms_category1` FOREIGN KEY (`cms_category_id`) REFERENCES `cms_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_post_created_by_user_id` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_post_updated_by_user_id` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
